# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from __future__ import annotations

from .mca_plot import McaPlotWidget


class OneDimDataPlotWidget(McaPlotWidget):
    def deviceName(self):
        # FIXME: This have to be saved in the configuration
        return self.windowTitle().split(" ")[0]
